﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridControlCalm : MonoBehaviour {

    //this script covers the creation of the ball grid
    //the grid ensures uniform distances between each ball

    //these public variables can be tweaked outside of the script
    public GameObject gridPiece;
    public int Width = 10;
    public int Height = 10;
    public float gridPieceX;
    private float incrementX;
    public float gridPieceY;
    

    //create the grid with its parameters
    private GameObject[,] grid = new GameObject[13, 13];


    //when the game starts...
    void Awake()
    {
        //GameObject.Find("BallPrefabGrid").GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
        //while there are still holes left to fill in the grid

        //
        for (int x = 0; x < Width; x++)
        {
            //reset the X coordinate back to its original position
            incrementX = gridPieceX;
            
            

            for (int y = 0; y < Height; y++)
            {
                //set the parameters for the ball
                gridPiece.transform.position = new Vector2(incrementX, gridPieceY);
                //create the ball
                grid[x,y] = (GameObject)Instantiate(gridPiece);
                //increase the coordinates by 0.7 each
                incrementX += 0.7f;           
                               
            }

            //increase the Y coordinate by 0.7
            gridPieceY += 0.7f;
            
        }

    }


    void Update()
    {
       //BallsCrumble();
    }



    
    ////////////////// BALLS FALLING WITH TIME LIMIT ///////////////////
    //this function is responsible for rendering that balls dynamic - hence, making them fall off the screen - after
    //the time reaches a certain limit


    void BallsCrumble()
    {
        //if there are only 60 seconds left
        if (GameObject.Find("MainManager").GetComponent<MainManager>().TimeLeft == 60)
        {
            Debug.Log("All these balls should fall now");


            //addressing all the balls on the first row

            for (int y = 0; y <= 9; y++)
            {
                //if the ball was already deleted, move on to the next one
                if (grid[0, y] == null)
                {
                    y++;
                }

                //make the balls of this row fall
                else
                {
                    grid[0, y].GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                    grid[0, y].GetComponent<CircleCollider2D>().enabled = false;
                }


            }
        }

        //if there are only 50 seconds left on the timer
        if (GameObject.Find("MainManager").GetComponent<MainManager>().TimeLeft == 50)
        {

            //addressing all the balls on the second row

            for (int y = 0; y <= 9; y++)
            {

                //if the ball was already deleted, move on to the next one
                if (grid[1, y] == null)
                {
                    y++;
                }

                //make the balls of this row fall
                else
                {
                    grid[1, y].GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                    grid[1, y].GetComponent<CircleCollider2D>().enabled = false;
                }

            }

        }

    }

}
