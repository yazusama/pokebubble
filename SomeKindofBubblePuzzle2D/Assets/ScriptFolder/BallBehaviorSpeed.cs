﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviorSpeed : MonoBehaviour {

    //this class works with the way the ball interacts with the walls and the other balls


	public string[] spr;
    public Transform StuckBall;

	public bool IsExploding = false;
	public int CurrentlyCollidingSameColorBalls = 0;
    public bool InCollision;
    public int xIndex;
    public int yIndex;
    public List<GameObject> AllSameColoredBallsCollision = new List<GameObject> ();
    private SpriteRenderer spriteRenderer;
    public int chosenSprite;
    public bool ballsExist;

    public string squareball;
    public string pentaball;
    public string triangleball;
    public string starball;
    public string goldenball;
    public string timeball;
    public string stuckball;




    // The prefab renders a random sprite from an array of sprite strings
    void Start() {

        spriteRenderer = GetComponent<SpriteRenderer>();
        float randomvalue = Random.value;

        //Debug.Log("random value is: " + randomvalue);

        if (randomvalue < 0.03)
        {
            spriteRenderer.sprite = Resources.Load("starBallGold", typeof(Sprite)) as Sprite; // set the sprite to starBallGold
            
        }
        else if (randomvalue < 0.05)
        {
            spriteRenderer.sprite = Resources.Load("timeBall", typeof(Sprite)) as Sprite; // set the sprite to timeBall
            
        }
        else if (randomvalue < 0.10)
        {
            spriteRenderer.sprite = Resources.Load("stuckBall", typeof(Sprite)) as Sprite; // set the sprite to stuckBall
           
        }

        else
        {
            chosenSprite = Random.Range(0, spr.Length);

            if (spriteRenderer.sprite == null)
            { // if the sprite on spriteRenderer is null then
                spriteRenderer.sprite = Resources.Load(spr[chosenSprite], typeof(Sprite)) as Sprite; // set the sprite to chosen ball
            }
            //Debug.Log("Chosen sprite index is: " + chosenSprite);

        }
	}





	// Update is called once per frame
	void Update () {
		
	}

    //when the ball collides with something

	void OnCollisionEnter2D(Collision2D col){

		//Debug.Log ("Ball has collided with " + col.transform.tag);

        //if the ball collides with a wall, it will freeze
        //basically turn all these parameters to 0
		if (col.transform.tag == "Wall") {
            InCollision = true;
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            GetComponent<Rigidbody2D>().gravityScale = 0;
            GetComponent<Rigidbody2D>().angularVelocity = 0f;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<AudioSource> ().Play ();

            //generate a new ball the second the other collides with something
			GameObject.Find ("CanonParent").GetComponent<CannonControl> ().CreateBall ();
		}

        //if the ball collides with another ball and InCollision is true, it will also freeze
        if (col.transform.tag == "Ball" && col.gameObject.GetComponent<BallBehaviorSpeed>().InCollision)
        {
            InCollision = true;
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            GetComponent<Rigidbody2D>().gravityScale = 0;
            GetComponent<Rigidbody2D>().angularVelocity = 0f;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;

            //generate a new ball
			GameObject.Find ("CanonParent").GetComponent<CannonControl> ().CreateBall ();
            
            //if the sprites of both the balls match, add to the counter that will make them disappear
			if (GetComponent<SpriteRenderer> ().sprite == col.transform.GetComponent<SpriteRenderer>().sprite) {
                Debug.Log ("This object has the same material as me");
				CurrentlyCollidingSameColorBalls++;
				AllSameColoredBallsCollision.Add(col.gameObject);
				
					Explode();
			
			}


            //////////// BONUS STAR BALL //////////////

            if (GetComponent<SpriteRenderer>().sprite.name == "starBall" && col.transform.GetComponent<SpriteRenderer>().sprite.name == "starBallGold")
            {
                //Debug.Log("This object has the same material as me");
                CurrentlyCollidingSameColorBalls++;
                AllSameColoredBallsCollision.Add(col.gameObject);

                Explode();
                }


            //////////// TIME BALL IS HIT ///////////////


            if (GetComponent<SpriteRenderer>().sprite && col.transform.GetComponent<SpriteRenderer>().sprite.name == "timeBall")
            {
                //Debug.Log("This object has the same material as me");
                CurrentlyCollidingSameColorBalls++;
                AllSameColoredBallsCollision.Add(col.gameObject);

                //if the time ball is hit with any ball, add 1 minute to the timer
                Explode();
                AddTime();
            }



            ////////// STUCK BALL IS HIT //////////////


            if (GetComponent<SpriteRenderer>().sprite && col.transform.GetComponent<SpriteRenderer>().sprite.name == "stuckBall")
            {
                //Debug.Log("This object has the same material as me");
                CurrentlyCollidingSameColorBalls++;
                AllSameColoredBallsCollision.Add(col.gameObject);

                //if the time ball is hit with any ball, freeze the canon rotation temporarily
                Explode();
                GameObject.Find("CanonParent").GetComponent<CannonControl>().FreezeCanon();
            }


        }

	}

    public void Explode() {
        Debug.Log("Time to explooooodeeee :)");


        IsExploding = true;

        if (GetComponent<SpriteRenderer>().sprite.name == triangleball) { 
        GameObject.Find("MainManager").GetComponent<MainManager>().TotalScore += 300;
    }
        if (GetComponent<SpriteRenderer>().sprite.name == squareball)
        {
            GameObject.Find("MainManager").GetComponent<MainManager>().TotalScore += 400;
        }
        if (GetComponent<SpriteRenderer>().sprite.name == pentaball)
        {
            GameObject.Find("MainManager").GetComponent<MainManager>().TotalScore += 500;
        }
        if (GetComponent<SpriteRenderer>().sprite.name == starball)
        {
            GameObject.Find("MainManager").GetComponent<MainManager>().TotalScore += 600;
        }
        if (GetComponent<SpriteRenderer>().sprite.name == goldenball)
        {
            GameObject.Find("MainManager").GetComponent<MainManager>().TotalScore += 1000;
        }

        //Tell my friends to explodee
        Debug.Log(AllSameColoredBallsCollision.Count);
		foreach (GameObject ball in AllSameColoredBallsCollision) {
			if (ball.GetComponent<BallBehaviorSpeed> ().IsExploding == false) {
				ball.GetComponent<BallBehaviorSpeed> ().Explode ();
			}
		}

		//Now I shall perish in the void
		Destroy (gameObject);

	}


    public void AddTime()
    {
        //add 60 seconds to the TimeLeft variable found in the MainManager
        GameObject.Find("MainManager").GetComponent<MainManager>().TimeLeft += 60;
    }

    


}
