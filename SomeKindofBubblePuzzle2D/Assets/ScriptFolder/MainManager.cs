﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class MainManager : MonoBehaviour
{

    //probably the most important class as it handles all the internal mechanics for the
    //overall game

    //score elements
    public int TotalScore;
    public int MinScore = 10000;
    public bool scoreAchieved = false;


    //type of gameplay
    public bool speedMode = true;



    //amount of seconds left at the start point
    public int TimeLeft = 30;

    //UI elements
    public GameObject GameOverPanel;
    public GameObject GameWinPanel;


    //determines whether the game is over or not
    private bool gameOver;




    // At the beginning of the game, all booleans are set to false
    // and the countdown begins
    void Start()
    {

        gameOver = false;

        GameOverPanel.SetActive(false);
        GameWinPanel.SetActive(false);
        Time.timeScale = 1;
        StartCoroutine(CountDown());

    }

    //when the timer reaches 0, the restart boolean will become true, and the player can now restart the game
    void Update()
    {
        GameWin();
        SpeedGameWin();

        //posts the score based on the player's performance
        GameObject.Find("ScoreLabel").GetComponent<Text>().text = "SCORE: " + TotalScore;

        //the minimum score for the speed round
        if (MinScore > 0)
        {
            GameObject.Find("MinimumScoreLabel").GetComponent<Text>().text = "MINIMUM SCORE: " + MinScore;
        }


        //here we are translating the time in seconds to something more comprehensible (in the form of an actual timer)
        System.TimeSpan time = System.TimeSpan.FromSeconds(TimeLeft);
        string answer = string.Format("{0:D2}m: {1:D2}s",
                time.Minutes,
                time.Seconds);

        //the timer will turn red if there are less than 60 seconds left
        if (TimeLeft <= 60)
        {
            GameObject.Find("TimeLabel").GetComponent<Text>().color = new Color(255, 0, 0);
        }
        else
        {
            GameObject.Find("TimeLabel").GetComponent<Text>().color = new Color(0, 0, 0);
        }

        GameObject.Find("TimeLabel").GetComponent<Text>().text = "TIME: " + answer;



    }

    //this just makes time move
    IEnumerator CountDown()
    {
        while (TimeLeft > 0)
        {
            //Debug.Log (TimeLeft);
            yield return new WaitForSeconds(1);
            TimeLeft--;
        }

        if (TimeLeft == 0)
        {
            GameOver();
        }

    }


    //Game Over Function
    //basically freezes the scene once the timer falls to 0
    public void GameOver()
    {

        if (!speedMode)
        {
            GameOverPanel.SetActive(true);
            gameOver = true;
            //Debug.Log("The Game is Over");
            Time.timeScale = 0;
        }

        //in speed mode, if the score is under the minimum, than that is considered a game over as well
        else if (speedMode && TotalScore < MinScore)
        {
            GameOverPanel.SetActive(true);
            gameOver = true;
            //Debug.Log("The Game is Over");
            Time.timeScale = 0;

        }

    }

    //for the calm mode, this is just to make sure there are no more balls in the scene, therefore considering it a proper win~
    public void GameWin()
    {
        //Debug.Log("This is the winning function!");
        if (!GameObject.Find("BallPrefabGrid(Clone)") && !GameObject.Find("BallPrefabGridSpeed(Clone)"))
        {
            //Debug.Log("There are no more balls in the scene.");
            GameWinPanel.SetActive(true);
            Time.timeScale = 0;

        }



    }



    //if the score is over the minimum, then the player wins!
    public void SpeedGameWin()
    {
        if (speedMode && TotalScore >= MinScore && TimeLeft == 0)
        {
            scoreAchieved = true;
            GameWinPanel.SetActive(true);
            Time.timeScale = 0;



        }

    }






}
