﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaySoundOnClick : MonoBehaviour {

    //plays sound when you click on something
    public void playSound()
    {
        GetComponent<AudioSource>().Play();
    }

}
