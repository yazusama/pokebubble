﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridControlSpeed : MonoBehaviour
{

    //this script covers the creation of the ball grid
    //the grid ensures uniform distances between each ball
    //it also deals with rows falling one after another after a few seconds pass

    //these public variables can be tweaked outside of the script
    public GameObject gridPiece;
    public int Width = 10;
    public int Height = 10;
    public float gridPieceX;
    private float incrementX;
    public float gridPieceY;
    public int timeLapse = 10;
    public int timeThreshold = 120;

    //array that associates the time thresholds with the row needed to be removed
    public int[] mapping = new int[11];


    //create the grid with its parameters
    private GameObject[,] grid = new GameObject[13, 13];


    //when the game starts...
    void Awake()
    {

        //while there are still holes left to fill in the grid



        int threshold = timeThreshold;

        for (int i = 0; i < 11; i++)
        {

            mapping[i] = threshold;
            threshold -= timeLapse;

            //Debug.Log("i: " + i + " threshold " + threshold);

        }

        for (int x = 0; x < Width; x++)
        {
            //reset the X coordinate back to its original position
            incrementX = gridPieceX;



            for (int y = 0; y < Height; y++)
            {
                //set the parameters for the ball
                gridPiece.transform.position = new Vector2(incrementX, gridPieceY);
                //create the ball
                grid[x, y] = (GameObject)Instantiate(gridPiece);
                //increase the coordinates by 0.7 each
                incrementX += 0.7f;

            }

            //increase the Y coordinate by 0.7
            gridPieceY += 0.7f;

        }

    }


    void Update()
    {
        BallsCrumble();
    }






    ////////////////// BALLS FALLING WITH TIME LIMIT ///////////////////
    //this function is responsible for rendering that balls dynamic - hence, making them fall off the screen - after
    //the time reaches a certain limit




    void BallsCrumble()
    {

        //bool deleteLines = true;
        int x = -1;



        //if there are only 60 seconds left


        //this code searches for the right threshold to match the time that is left and the row # associated with that
        for (int i = 0; i < 11; i++)
        {
            int threshold = mapping[i];

            //if the amount of time left on the timer matches the benchmark time, remove the balls of the designated row
            if (GameObject.Find("MainManager").GetComponent<MainManager>().TimeLeft == threshold)
            {
                x = i;
                break;
            }

            //Debug.Log("i: " + i + " threshold " + threshold + " x: " + x + " timeLeft: " + GameObject.Find("MainManager").GetComponent<MainManager>().TimeLeft);

        }

        //Debug.Log("x:" + x);

        if (x >= 0 && x < 11)
        {
            //Debug.Log("All these balls should fall now");
            if (GameObject.Find("MainManager").GetComponent<MainManager>().TimeLeft == mapping[x])
            {
                //addressing all the balls on the first row

                for (int y = 0; y <= 9; y++)
                {
                    //if the ball was already deleted, move on to the next one
                    if (grid[x, y] == null)
                    {
                        y++;
                    }

                    //make the balls of this row fall
                    else
                    {
                        grid[x, y].GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                        grid[x, y].GetComponent<CircleCollider2D>().enabled = false;
                    }

                    //Debug.Log("timeThreshold: " + timeThreshold + " x: " + x + " y: " + y);
                }

            }

        }

    }


}
