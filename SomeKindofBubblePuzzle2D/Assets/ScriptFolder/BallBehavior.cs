﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BallBehavior : MonoBehaviour
{

    //this class works with the way the ball interacts with the walls and the other balls

    //array of strings naming each sprite
    public string[] spr;
    public Transform StuckBall;

    public bool IsExploding = false;
    public int CurrentlyCollidingSameColorBalls = 0;
    public bool InCollision;
    public int xIndex;
    public int yIndex;
    public List<GameObject> AllSameColoredBallsCollision = new List<GameObject>();
    private SpriteRenderer spriteRenderer;
    public int chosenSprite;
    public bool ballsExist;

    //all the strings matched with the strings in the array above
    public string squareball;
    public string pentaball;
    public string triangleball;
    public string starball;
    public string goldenball;
    public string timeball;

    public Sprite[] frames = new Sprite[10];
    int framesPerSecond = 60;



    // The prefab renders a random sprite from an array of sprite strings
    void Start()
    {

        spriteRenderer = GetComponent<SpriteRenderer>();
        float randomvalue = Random.value;

        //Debug.Log("random value is: " + randomvalue);

        //this decreases the chances of a starBallGold appearing
        if (randomvalue < 0.03)
        {
            spriteRenderer.sprite = Resources.Load("starBallGold", typeof(Sprite)) as Sprite; // set the sprite to starBallGold

        }
        //this decreases the chances of a timeBall appearing
        else if (randomvalue < 0.05)
        {
            spriteRenderer.sprite = Resources.Load("timeBall", typeof(Sprite)) as Sprite; // set the sprite to timeBall

        }

        //this deals with the generation of all the other sprites
        else
        {
            chosenSprite = Random.Range(0, spr.Length);

            if (spriteRenderer.sprite == null)
            { // if the sprite on spriteRenderer is null then
                spriteRenderer.sprite = Resources.Load(spr[chosenSprite], typeof(Sprite)) as Sprite; // set the sprite to chosen ball
            }
            //Debug.Log("Chosen sprite index is: " + chosenSprite);

        }
    }



    //when the ball collides with something

    void OnCollisionEnter2D(Collision2D col)
    {

        //Debug.Log ("Ball has collided with " + col.transform.tag);

        //if the ball collides with a wall, it will freeze
        //basically turn all these parameters to 0
        if (col.transform.tag == "Wall")
        {
            InCollision = true;
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            GetComponent<Rigidbody2D>().gravityScale = 0;
            GetComponent<Rigidbody2D>().angularVelocity = 0f;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;
            GetComponent<AudioSource>().Play();

            //generate a new ball the second the other collides with something
            GameObject.Find("CanonParent").GetComponent<CannonControl>().CreateBall();
        }

        //if the ball collides with another ball and InCollision is true, it will also freeze
        if (col.transform.tag == "Ball" && col.gameObject.GetComponent<BallBehavior>().InCollision)
        {
            InCollision = true;
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            GetComponent<Rigidbody2D>().gravityScale = 0;
            GetComponent<Rigidbody2D>().angularVelocity = 0f;
            GetComponent<Rigidbody2D>().velocity = Vector2.zero;

            //generate a new ball
            GameObject.Find("CanonParent").GetComponent<CannonControl>().CreateBall();

            //if the sprites of both the balls match, add to the counter that will make them disappear
            if (GetComponent<SpriteRenderer>().sprite == col.transform.GetComponent<SpriteRenderer>().sprite)
            {
                Debug.Log("This object has the same material as me");
                CurrentlyCollidingSameColorBalls++;
                AllSameColoredBallsCollision.Add(col.gameObject);

                Explode();

            }


            //////////// BONUS STAR BALL //////////////

            if (GetComponent<SpriteRenderer>().sprite.name == "starBall" && col.transform.GetComponent<SpriteRenderer>().sprite.name == "starBallGold")
            {
                //Debug.Log("This object has the same material as me");
                CurrentlyCollidingSameColorBalls++;
                AllSameColoredBallsCollision.Add(col.gameObject);

                Explode();
            }


            //////////// TIME BALL IS HIT ///////////////


            if (GetComponent<SpriteRenderer>().sprite && col.transform.GetComponent<SpriteRenderer>().sprite.name == "timeBall")
            {
                //Debug.Log("This object has the same material as me");
                CurrentlyCollidingSameColorBalls++;
                AllSameColoredBallsCollision.Add(col.gameObject);

                //if the time ball is hit with any ball, add 1 minute to the timer
                Explode();
                AddTime();
            }


        }

    }


    //Explode function
    //if the ball collides with anything of the same nature, it will "explode"
    //this function deals with the various things we want the ball to do before we bid it farewell
    public void Explode()
    {
        //Debug.Log("Time to explooooodeeee :)");


        IsExploding = true;


        //all these if statements deal with scoring
        if (GetComponent<SpriteRenderer>().sprite.name == triangleball)
        {
            GameObject.Find("MainManager").GetComponent<MainManager>().TotalScore += 300;
        }
        if (GetComponent<SpriteRenderer>().sprite.name == squareball)
        {
            GameObject.Find("MainManager").GetComponent<MainManager>().TotalScore += 400;
        }
        if (GetComponent<SpriteRenderer>().sprite.name == pentaball)
        {
            GameObject.Find("MainManager").GetComponent<MainManager>().TotalScore += 500;
        }
        if (GetComponent<SpriteRenderer>().sprite.name == starball)
        {
            GameObject.Find("MainManager").GetComponent<MainManager>().TotalScore += 600;
        }
        if (GetComponent<SpriteRenderer>().sprite.name == goldenball)
        {
            GameObject.Find("MainManager").GetComponent<MainManager>().TotalScore += 1000;
        }

        //Tell my friends to explodee
        //Debug.Log(AllSameColoredBallsCollision.Count);
        foreach (GameObject ball in AllSameColoredBallsCollision)
        {
            if (ball.GetComponent<BallBehavior>().IsExploding == false)
            {
                ball.GetComponent<BallBehavior>().Explode();
            }
        }


        //I TRIED to add an animation, but it seems like it didn't work...
        destroyAnimation();


        //Now I shall perish in the void
        Destroy(gameObject);

    }

    //This function add 1 minute to the timer once you collide with a timer ball
    public void AddTime()
    {
        //add 60 seconds to the TimeLeft variable found in the MainManager
        GameObject.Find("MainManager").GetComponent<MainManager>().TimeLeft += 60;
    }


    //this function supposedly creates an animation
    //but it doesn't seem to work...
    public void destroyAnimation()
    {
        int index = 0;

        while (index < 10)
        {
            GetComponent<SpriteRenderer>().sprite = frames[index];
            index++;
            // Debug.Log("frame: " + frames[index]);

        }
    }


}
