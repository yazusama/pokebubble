﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;

public class StateManager : MonoBehaviour
{

    //this code takes care of the logisitics behind the image unlocking in the calm mode
    //the player wins a round, and that transfers over to an array of booleans
    //if all the booleans in the array are true, the player unlocks the whole image
    //and wins the level

    //type of gameplay
    public bool speedMode = false;

    //array of booleans needed to unlock the images
    public bool[] wonRound = new bool[6];
    public bool wonAllRounds = true;

    //int keeping track of each boolean
    public int currentID;


    //makes sure that this code won't be reloaded each round
    private void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
    }



    void Update()
    {
        //function responsible for the player's victory
        GameWin();

        //means of verification
        wonAllRounds = true;

    }


    //GAME WIN
    //if there is not a single ball in the round, then the player has won the round!

    public void GameWin()
    {
        //Debug.Log("This is the winning function!");
        if (!GameObject.Find("BallPrefabGrid(Clone)") && !GameObject.Find("BallPrefabGridSpeed(Clone)"))
        {
            //Debug.Log("There are no more balls in the scene.");

            //this ID matches with the image of the round in question
            currentID = GameObject.FindGameObjectWithTag("Identifier").GetComponent<SceneState>().imageID;

            //Debug.Log("currentID: " + GameObject.FindGameObjectWithTag("Identifier").GetComponent<SceneState>().imageID);

            //and the winning of the round is associated with that ID
            wonRound[currentID] = true;

            //for loop that checks if all booleans are true
            for (int i = 0; i < 6; i++)
            {
                if (wonRound[i] == false)
                {
                    wonAllRounds = false;
                    break;
                }

            }


        }



    }










}
