﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageChange : MonoBehaviour
{

    //script responsible for swapping the locked image in the level select
    //with the unlocked one
    //still needs work...

    public Sprite lockedImage;
    public Sprite unlockedImage;
    //public Sprite unlockedImageHighlighted;
    public int imageNum;
    public bool unlocked;

    private void Awake()
    {
        //DontDestroyOnLoad(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {

        GetComponent<Image>().sprite = lockedImage;

        if (GameObject.Find("StateManager").GetComponent<StateManager>().currentID == imageNum && GameObject.Find("StateManager").GetComponent<StateManager>().wonRound[GameObject.Find("StateManager").GetComponent<StateManager>().currentID] == true)
        {
            unlocked = true;
        }

        if (unlocked && GetComponent<Image>().sprite == lockedImage)
        {
            GetComponent<Image>().sprite = unlockedImage;
        }

    }
}
