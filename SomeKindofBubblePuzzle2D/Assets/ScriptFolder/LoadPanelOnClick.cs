﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadPanelOnClick : MonoBehaviour
{

    public GameObject currentPanel;
    public GameObject nextPanel;

    public void TogglePanel(GameObject nextPanel)
    {
        nextPanel.SetActive(!nextPanel.activeSelf);
        currentPanel.SetActive(false);
        
    }
}