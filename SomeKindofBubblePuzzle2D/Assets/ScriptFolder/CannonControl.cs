﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CannonControl : MonoBehaviour
{

    //speed of rotation
    public float RotateSpeed;

    public Rigidbody2D BallPrefab;
    public Transform SpawnPoint;
    public float ShootForce;
    public Renderer CanonRenderer;
    Rigidbody2D NewBall;

    //checking if a ball is already on the top of the cannon
    bool gotball = false;
    bool frozen = false;

    //amount of time in which the cannon is frozen if it hits a stuck ball
    int timeFrozen = 5;

    public Sprite background;


    //will create a ball at the top of the cannon right when the scene loads
    void Start()
    {

        CreateBall();


    }

    // Deals with rotating the canon with the arrow keys and spawning the balls
    void Update()
    {

        //if you press the right key, the canon will rotate to the right

        if ((Input.GetKey(KeyCode.RightArrow) == true) && !frozen)
        {

            //you cant rotate it beyond a certain degree
            if (transform.rotation.z > -0.7)
            {
                transform.Rotate(new Vector3(0, 0, -RotateSpeed));
            }
            else
            {

                transform.Rotate(new Vector3(0, 0, 0));
            }

        }

        //if you press the right key, the canon will rotate to the right
        if ((Input.GetKey(KeyCode.LeftArrow) == true) && !frozen)
        {
            //you cant rotate it beyond a certain degree
            if (transform.rotation.z < 0.7)
            {
                transform.Rotate(new Vector3(0, 0, RotateSpeed));
            }
            else
            {

                transform.Rotate(new Vector3(0, 0, 0));
            }

        }

        //when you press space, a ball will shoot, and the cannon will generate a new ball
        if (Input.GetKeyDown(KeyCode.Space) == true)
        {



            NewBall.transform.parent = null;
            NewBall.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

            Vector2 DirectionVector = SpawnPoint.position - transform.position;

            DirectionVector = DirectionVector * ShootForce;

            NewBall.GetComponent<Rigidbody2D>().AddForce(DirectionVector);

            Debug.Log(DirectionVector);
            GetComponent<AudioSource>().Play();
            gotball = false;

        }



    }

    //this function is responsible for creating the new ball
    public void CreateBall()
    {
        if (!gotball)
        {
            //Debug.Log ("creatingball");
            NewBall = Instantiate(BallPrefab, SpawnPoint.position, Quaternion.Euler(new Vector3(0, 0, 0))) as Rigidbody2D;
            NewBall.transform.parent = gameObject.transform;
            NewBall.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
            gotball = true;
        }

    }

    //These two functions deal with the cannon being frozen once a ball hits a stuck ball
    public void FreezeCanon()
    {
        StartCoroutine(FreezeCountDown());

    }

    IEnumerator FreezeCountDown()
    {
        //Debug.Log("The canon needs to stop moving right now!!");
        timeFrozen = 10;
        while (timeFrozen > 0)
        {

            frozen = true;
            GameObject.Find("Canon").GetComponent<SpriteRenderer>().color = new Color(255, 0, 0);
            yield return new WaitForSeconds(1);
            timeFrozen--;

        }
        frozen = false;
        GameObject.Find("Canon").GetComponent<SpriteRenderer>().color = new Color(255, 255, 255);
    }

}
